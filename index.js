const choo = require('choo')
const devtools = require('choo-devtools')

const cast = require('./views/cast')

// Initialize choo and bring along it's console helper.
var app = choo()
app.use(devtools())

// Our Stores
app.use(require('./stores/core'))

// Our Routes
app.route('/', cast)

//Plant that seed.
app.mount('body')
