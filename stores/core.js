const smarkt = require('smarkt')
const _ = require('lodash')
var archive = new DatArchive(window.location)

module.exports = (state, emitter) => {
  state.info = {}
  state.bios = []
  state.episode = {}

  emitter.on('DOMContentLoaded', () => {
    grabTxtsFromDir('show')
    checkForBios('show/bios')
  })

  function grabTxtsFromDir (dir) {
    archive.readdir(dir)
      .then(files => {
        var txts = onlyTxts(files)
        emitter.emit('Texts Found', txts)
      })
  }

  emitter.on('Texts Found', (texts) => {
    for (var text of texts) {
      emitter.emit('Map Text To State', text)
    }
  })
  emitter.on('Bios Found!', () => {
    archive.readdir('show/bios')
      .then(files => {
        var bios = onlyTxts(files)
        for (var bio of bios) {
          mapBioToState(bio)
        }
      })
  })

  emitter.on('Map Text To State', (text) => {
    mapTextToState(text)
  })

  function mapTextToState (text) {
    var textName = text.split('.txt')[0]
    archive.readFile(`show/${text}`)
      .then((contents) => {
        state[textName] = smarkt.parse(contents)
        emitter.emit('render')
      })
  }


  function checkForBios (dir) {
    archive.stat(dir)
      .then(success => emitter.emit('Bios Found!'))
      .catch(data => emitter.emit('no bio'))
  }

  function mapBioToState (bio) {
    archive.readFile(`show/bios/${bio}`)
      .then(content => {
        var bioJSON = smarkt.parse(content)
        state.bios = [...state.bios, bioJSON]
        emitter.emit('render')
      })
  }

  function onlyTxts(dir) {
    return dir.filter(file => file.includes('.txt'))
  }
}
