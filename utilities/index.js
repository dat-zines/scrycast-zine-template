function hasContent (state) {
  return state !== null && Object.keys(state).length !== 0
}

function humbleAndDash (str) {
  return str.toLowerCase().replace(' ','-').trim()
}

function keyPrompt (peer) {
  prompt(`${peer.name}'s SSB Key:`, peer.ssbKey)
}

module.exports = {hasContent, humbleAndDash, keyPrompt}
