title: solarpunk, travel, and metaphors
----
number: 02
----
seed date: 14 September 2018
----
localHost:
  name: Dan Hassan
  ssbKey: '@NeB4q4Hy9IiMxs5L08oevEhivxW+/aDu/s/0SkNayi0=.ed25519'
----
audio: the-local-gossip-episode-2.opus
----
image: 
  src: solarpunk-couple.jpg
  caption: 'The two peers at the Solarpunk Magic Computer Club in Queens'
----
peers:
  - name: Angelica Blevins
    ssbKey: '@eANNuLfzX/9rlGODXHYV8WJb+zw2h+d7YsT4vpYPvD0=.ed25519'
  - name: Zach Mandeville
    ssbKey: '@ZqH7Mctu/7DNInxuwl12ECjfrAKUX2tBLq1rOldNhg0=.ed25519'
