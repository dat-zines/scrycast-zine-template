name: Zach Mandeville
----
bio:
Writer, Coder, Barber, Tarotologist, and potential friend. In this episode, Dan dubbed him Scuttlebutt's poet laureate, which made Zach make a weird mouth sound and his face turned such a deep red you can hear it through yr speakers.

Here's some more of Zach's work in the Chorus:
- [The Future Will be Technical](dat://coolguy.website/writing/the-future-will-be-technical/)
- [A Praise Chorus](dat://2295a89c2cdfb57ed91b135608627119199d5d834fbaede70a8713b2cedf6fe1/)
- [Dat Zine Library](dat://coolguy.website/projects/dat-zine-library/)
- [His Home online](dat://coolguy.website)
