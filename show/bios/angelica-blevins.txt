name: Angelica Blevins
----
bio:

Cartoonist, zinester, yogi, and kitchen witch. Angelica has provided a unique visual element to our scuttlepelago through her ssb emojis and the art she's shared throughout the Chorus.

You can find an example of her yoga zines here: [Kundalini Winter Workshop](dat://81f40a285e2e7706cae59db182a4f4753df24f9c033cd1aa67e743a5f52f9008/).  And check out her homepage at [angblev.com](https://angblev.com)
