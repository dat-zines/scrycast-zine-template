# local gossip zine template

Hey hey, you got yrself scrycast zine!  This is definitely a WORK IN PROGRESS and it is likely that the names for things will change to be more clear. When that happens, i'll update the readme!

## QUICK START!
If you grabbed this template cos you just put out a new episode and wanna share it, then do this!:
- Go to the `show/` directory
- put any info about your scrycast itself into `info.txt`, following the format you see (like changing all the values to be relevant to you, but to not change the name of each section).
- Add any image you'd like to the shows/ folder, named whatever you like.
- Add the audio right into this folder, named whatever you like.
- in the `episode.txt` file, change the image and audio section to point to whatever files you added (this means you can have them point to external url's too.)
- In the bios folder add bios for each of your peers, following the format you see here
  ```
  name: their name
  ----
  bio: a bio written in markdown.
  ```
  For this, the files can be called whatever but it is important that they are .txt files and that the name section matches the peer's name section in `episode.txt`

## Intentions

This zine template is intended to make it easy for the scrycast make to put out new show pages for each of their episodes, without having to re-code a bunch of html.  Instead, you fork, manage a few text files within the beaker browser, put in the new audio, and hit publish.

It's also intended to invite collaboration.  If you add the show to a gitlab/git-ssb page, then people can make pull requests for each of the show files to put in their own stuff.  For example, if you want richer bios for each guest just have them edit their individual files on the git page, pull those changes, and hit publish.

It's intended to be entirely yr own, even if you are not yet deep into coding (or don't feel like being so at the moment).  You can change all content of the zine without any coding knowledge, through the files.  You can also chagne the aesthetic of the zine through simple css variables.  If you want to change the structure, you can do so using choo, a v. light and cute and accessible web framework.

This particular template is intended to be personal and transparent too.  I wrote it entirely in an org-file using 'literate programming' which means 'coding as a form of diary entry'.  If you have [spacemacs](http://spacemacs.org/), you can open this org file and see my entire working progress and why I did the thangs I did.  If you have not used choo before, this could be a good entrance as I tried to explain the logic and patterns for all the code.  I'll continue to work through this org file, to make it ever more personal and clear.

# Dreams
- The whole thing is properly styled to a degree I like (it's 30% there now.)
- The transcription section will become it's own file too, so if people wanna transcribe stuff they can just add directly to the file (through git)
- the page changes so that if there's no transcription it says 'please add one!' and if there is one, it's displayed.
- There's an annotation file so people can throw in links for stuff mentioned at particular points of the scrycast.
- There's a 'contribute' section in the episode file that points to whichver git link you want so people know where to contribute.
- the ssb key displays upon hover or some other magic interaction (it looks kindda strange with it just listed after the person's name.)
- annotations pop up based on timestamp (this would be coooooool)
- people like it and one day I get recommended a scrycast I've never heard of and when i go to their page I recognize traces of our code, but this person has made it entirely their own.
