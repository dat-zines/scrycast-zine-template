const html = require('nanohtml')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

const u = require('../utilities')
const displayBioIfAvailable = require('./peerBio')

module.exports = (state, emit) => {
  if(u.hasContent(state.episode)) {
  const peers = state.episode.peers
  return html`
    <section id='Bios'>
     <h2>More about our Peers</h2>
     <ul id='bio-list'>
     ${listBios(peers)}
    </section>
    `
  }
  function listBios (peers) {
  return peers.map(peer => {
    return html`
    <li class='peerBio'>
      ${checkForBio(state, peer)}
    </list>
    `
  })
  }

  function checkForBio (state, peer) {
    if (u.hasContent(state.bios)) {
      return displayBioIfAvailable(state.bios, peer)
    }
  }
}
