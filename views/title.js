const html = require('nanohtml')
const u = require('../utilities')

module.exports = (state, emit) => {
  if (u.hasContent(state.info)) {
    var info = state.info
    return html`
    <div id='Title'>
      <h1>${info.scrycast}</h1>
      <h2>${info.tagline}</h2>
      <p>${info.notes}</p>
    </div>
  `
  }
}
