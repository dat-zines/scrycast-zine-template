const html = require('nanohtml')
const PeerList = require('./peerList.js')
const AudioPlayer = require('./audioPlayer')
const u = require('../utilities')

module.exports = (state, emit) => {
  if (u.hasContent(state.episode)) {
    var episode = state.episode
    return html`
            <section id='Episode'>
            <h2>Episode ${episode.number}: <span id='episode-title'>${episode.title}</span></h2>
            ${AudioPlayer(episode.audio)}
            <p id='seed-date'>Initial Seeding Date: ${episode['seed date']}</p>
            <div class='episode-image'>
            <img src='show/${episode.image.src}' alt='${episode.image.caption}' />
            </div>
            <figcaption>${episode.image.caption}</figcaption>
            <h3 id='localHost'>Localhost</h3>
            <p>${episode.localHost.name}
            <button onclick=${() => u.keyPrompt(episode.localHost)} href=false>${'🔑'}</button></p>
            <h3>Peers</h3>
            <ul id='peerList'>
              ${PeerList(state)}
            </ul>
            </section>
          `
        }
  function copyToClipboard (peer) {
    prompt(`the SSB Key for ${peer.name} is:`, peer.ssbKey)
  }
}
