const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.episode) {
    return html`
    <section id='header'>
      <h1>${state.show.scrycast}</h1>
      <h2>Episode ${state.episode.number}: ${state.episode.title}</h2>
    </section>
  `
  }
}
