const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.transcript) {
    return html`
    <div>
    <h2>Transcription</h2>
    ${state.transript}
    </div>
`
  } else {
    return html`
     <div>
       <h2>Transcription</h2>
       <p>This show hasn't been transcribed yet, but we'd love it to be!  If you'd like to help with this, add a transcript file to our show folder on our [gitlab page](https://gitlab.com/zachmandeville/scrycast-zine)</p>
     </div>
`
   }
}
