const html = require('nanohtml')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

const u = require('../utilities')
const displayBioIfAvailable = require('./peerBio')

module.exports = function peerList (state) {
  if(u.hasContent(state.episode.peers)) {
  const peers = state.episode.peers
  return peers.map(peer => {
    return html`
    <li class='peerCard'>
      ${peer.name} <button onclick=${() => u.keyPrompt(peer)}>${'🔑'}</button>
    </li>
    `
  })
  }
}
