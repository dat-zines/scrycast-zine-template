const html = require('nanohtml')

const Title = require('./title')
const Episode = require('./episode')
const Transcription = require('./transcription')
const Bios = require('./peerBioList')


module.exports = (state, emit) => {
  return html`
  <body>
    <div class='wrapper'>
      ${Title(state,emit)}
      ${Episode(state, emit)}
      ${Bios(state, emit)}
      ${Transcription(state, emit)}
    </div>
  </body>
  `
}
