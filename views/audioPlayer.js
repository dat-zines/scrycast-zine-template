const html = require('nanohtml')

module.exports = (audio) => {
  return html`
    <audio class='Player' style='width: 400px' controls src='show/${audio}'></audio>
`
}
