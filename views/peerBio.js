const html = require('nanohtml')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

const u = require('../utilities')

module.exports = (bios, peer) => {
  if(hasBio(bios, peer)) {
    var bio = grabBio(bios, peer)
    var idName = u.humbleAndDash(peer.name)
    return html`
    <div class='bio' id='bio_${idName}'>
      <h1>${peer.name}</h1>
      ${raw(md.render(bio.bio))} 
    </div>
    `
  }

  function hasBio (bios, peer) {
    return bios.some(bio => {
      var bioName = bio.name.toLowerCase()
      var peerName = peer.name.toLowerCase()
      return peerName === bioName
    })
  }

  function grabBio (bios, peer) {
    return bios.find(bio => {
      var bioName = bio.name.toLowerCase()
      var peerName = peer.name.toLowerCase()
      return peerName === bioName
    })
  }
}
