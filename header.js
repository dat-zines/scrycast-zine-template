const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.info !== {}) {
  return html`
    <section id='header'>
      <h1>${state.info.scrycast}</h1>
      <h2>Episode ${state.info.episode}: ${state.info.episodeTitle}</h2>
    </section>
  `
}
